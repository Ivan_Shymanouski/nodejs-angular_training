﻿var request = require('supertest'),
	app = require('../server.js').app;

describe('Authorization: response', function() {
	it('get login.html should response 200 without token', function (done) {
		request(app)
			.get('/views/authorization/login.html')
			.expect('Content-Type', /text\/html/)
			.expect(200, done);
	});
	it('get registration.html should response 200 without token', function (done) {
		request(app)
			.get('/views/authorization/registration.html')
			.expect('Content-Type', /text\/html/)
			.expect(200, done);
	});
	it('get game.html should response 302 to login without token', function (done) {
		request(app)
			.get('/game/game.html')
			.expect('Content-Type', /text\/plain; charset=utf-8/)
			.expect('Location', /..\/views\/authorization\/login.html/)
			.expect(302, done);
	});
	it('get game.html should response 200 with Larets token', function (done) {
		var validToken = "BingoShimToken=%22eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyIkX18iOnsic3RyaWN0TW9kZSI6dHJ1ZSwiZ2V0dGVycyI6e30sIndhc1BvcHVsYXRlZCI6ZmFsc2UsImFjdGl2ZVBhdGhzIjp7InBhdGhzIjp7Il9fdiI6ImluaXQiLCJfaWQiOiJpbml0IiwibG9naW4iOiJpbml0IiwicGFzc3dvcmQiOiJpbml0IiwiZW1haWwiOiJpbml0In0sInN0YXRlcyI6eyJpZ25vcmUiOnt9LCJkZWZhdWx0Ijp7fSwiaW5pdCI6eyJfX3YiOnRydWUsIl9pZCI6dHJ1ZSwibG9naW4iOnRydWUsInBhc3N3b3JkIjp0cnVlLCJlbWFpbCI6dHJ1ZX0sIm1vZGlmeSI6e30sInJlcXVpcmUiOnt9fSwic3RhdGVOYW1lcyI6WyJyZXF1aXJlIiwibW9kaWZ5IiwiaW5pdCIsImRlZmF1bHQiLCJpZ25vcmUiXX0sImVtaXR0ZXIiOnsiZG9tYWluIjpudWxsLCJfZXZlbnRzIjp7fSwiX2V2ZW50c0NvdW50IjowLCJfbWF4TGlzdGVuZXJzIjowfX0sImlzTmV3IjpmYWxzZSwiX2RvYyI6eyJfX3YiOjAsIl9pZCI6IjU3MjBkYWI2ODQzNzc2ZTgxYjEyYmJjMyIsImxvZ2luIjoiTGFyZXRzIiwicGFzc3dvcmQiOiIxMjM0NTZBYSIsImVtYWlsIjoic2hpbV9pdmFuQGVwYW0uY29tIn0sIl9wcmVzIjp7IiRfX29yaWdpbmFsX3NhdmUiOltudWxsLG51bGxdfSwiX3Bvc3RzIjp7IiRfX29yaWdpbmFsX3NhdmUiOltdfSwiaWF0IjoxNDYyMjkwODk4LCJleHAiOjE0NjIzMTYwOTh9.5LmclrQVZ0mXLxV73sI8usR4cZwV53NBhclWu4LcUYY%22";
		console.log('if next one fail you can put into token of user which exist')
		request(app)
			.get('/game/game.html')
			.set('Cookie', validToken)
			.expect(200, done);
	});
	it('get game.html should response 302 with wrong token', function (done) {
		var invalidToken = "BingoShimToken=%2eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyIkX18iOnsic3RyaWN0TW9kZSI6dHJ1ZSwiZ2V0dGVycyI6e30sIndhc1BvcHVsYXRlZCI6ZmFsc2UsImFjdGl2ZVBhdGhzIjp7InBhdGhzIjp7Il9fdiI6ImluaXQiLCJfaWQiOiJpbml0IiwibG9naW4iOiJpbml0IiwicGFzc3dvcmQiOiJpbml0IiwiZW1haWwiOiJpbml0In0sInN0YXRlcyI6eyJpZ25vcmUiOnt9LCJkZWZhdWx0Ijp7fSwiaW5pdCI6eyJfX3YiOnRydWUsIl9pZCI6dHJ1ZSwibG9naW4iOnRydWUsInBhc3N3b3JkIjp0cnVlLCJlbWFpbCI6dHJ1ZX0sIm1vZGlmeSI6e30sInJlcXVpcmUiOnt9fSwic3RhdGVOYW1lcyI6WyJyZXF1aXJlIiwibW9kaWZ5IiwiaW5pdCIsImRlZmF1bHQiLCJpZ25vcmUiXX0sImVtaXR0ZXIiOnsiZG9tYWluIjpudWxsLCJfZXZlbnRzIjp7fSwiX2V2ZW50c0NvdW50IjowLCJfbWF4TGlzdGVuZXJzIjowfX0sImlzTmV3IjpmYWxzZSwiX2RvYyI6eyJfX3YiOjAsIl9pZCI6IjU3MjBkYWI2ODQzNzc2ZTgxYjEyYmJjMyIsImxvZ2luIjoiTGFyZXRzIiwicGFzc3dvcmQiOiIxMjM0NTZBYSIsImVtYWlsIjoic2hpbV9pdmFuQGVwYW0uY29tIn0sIl9wcmVzIjp7IiRfX29yaWdpbmFsX3NhdmUiOltudWxsLG51bGxdfSwiX3Bvc3RzIjp7IiRfX29yaWdpbmFsX3NhdmUiOltdfSwiaWF0IjoxNDYyMDQ2MTU4LCJleHAiOjE0NjIwNzEzNTh9.W76Etms-4d6BTMn-X5L7Y6BxWADW6b7SYrAlQtX-VPA%22";
										  
		request(app)
			.get('/game/game.html')
			.set('Cookie', invalidToken)
			//.expect('Location', /..\/views\/authorization\/login.html/)
			.expect(302, done);
	});
	it('get game.html response 200 with unexist user token', function (done) {
		var unexistValidToken = "BingoShimToken=%22eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyIkX18iOnsic3RyaWN0TW9kZSI6dHJ1ZSwiZ2V0dGVycyI6e30sIndhc1BvcHVsYXRlZCI6ZmFsc2UsImFjdGl2ZVBhdGhzIjp7InBhdGhzIjp7Il9fdiI6ImluaXQiLCJfaWQiOiJpbml0IiwibG9naW4iOiJpbml0IiwicGFzc3dvcmQiOiJpbml0IiwiZW1haWwiOiJpbml0In0sInN0YXRlcyI6eyJpZ25vcmUiOnt9LCJkZWZhdWx0Ijp7fSwiaW5pdCI6eyJfX3YiOnRydWUsIl9pZCI6dHJ1ZSwibG9naW4iOnRydWUsInBhc3N3b3JkIjp0cnVlLCJlbWFpbCI6dHJ1ZX0sIm1vZGlmeSI6e30sInJlcXVpcmUiOnt9fSwic3RhdGVOYW1lcyI6WyJyZXF1aXJlIiwibW9kaWZ5IiwiaW5pdCIsImRlZmF1bHQiLCJpZ25vcmUiXX0sImVtaXR0ZXIiOnsiZG9tYWluIjpudWxsLCJfZXZlbnRzIjp7fSwiX2V2ZW50c0NvdW50IjowLCJfbWF4TGlzdGVuZXJzIjowfX0sImlzTmV3IjpmYWxzZSwiX2RvYyI6eyJfX3YiOjAsIl9pZCI6IjU3MjhjYmRlNjBmZjJmOWMwZjViMzM4NSIsImxvZ2luIjoic2FkZiIsInBhc3N3b3JkIjoicXdlYWRRV0UxMjMiLCJlbWFpbCI6InF3ZUBxd2UucXdlIn0sIl9wcmVzIjp7IiRfX29yaWdpbmFsX3NhdmUiOltudWxsLG51bGxdfSwiX3Bvc3RzIjp7IiRfX29yaWdpbmFsX3NhdmUiOltdfSwiaWF0IjoxNDYyMjkxNDI0LCJleHAiOjE0NjIzMTY2MjR9.3QOauA9EGjDiYp9CvVyLSiVVjzcklaya2_kIrayxio8%22";
		console.log('ToDo: next should be correct : 302 to login or registration');
		request(app)
			.get('/game/game.html')
			.set('Cookie', unexistValidToken)
			//.expect('Location', /..\/views\/authorization\/login.html/)
			.expect(200, done);
	});
});