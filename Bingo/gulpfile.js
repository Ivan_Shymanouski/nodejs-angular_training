﻿var gulp = require('gulp'),
    useref = require('gulp-useref'),
    gulpif = require('gulp-if'),
    //uglify = require('gulp-uglify'), // Минификация JS
	//minifyJS = require('gulp-minify'); // Минификация JS
	//uglify = require('gulp-uglify');
    minifyCss = require('gulp-minify-css'), // Минификация CSS
    nodemon = require('gulp-nodemon'),
    browserSync = require('browser-sync'),
    del = require('del'),
    runSequence = require('run-sequence');

//--content
var srcIndex = 'front_end/views/index.html',
    dstIndex = 'public',
    srcIcon = 'front_end/images/favicon.ico',
    srcImages = ['front_end/images/**/*', '!' + srcIcon],
    dstImages = dstIndex + '/images',
    gamePath = 'front_end/views/{game/**,game}',
    srcViews = ['front_end/views/**/*', '!' + gamePath, '!' + srcIndex],
    dstViews = dstIndex + '/views',
    //--JS
    srcJSlib = 'front_end/js/*.js',
	srcJSFrameworksFoulder = 'front_end/js/custom/jsFrameworks',
    srcAngularJS = 'front_end/js/custom/jsFrameworks/angular/**/*.js',
    srcJS = ['front_end/js/custom/**/*.js', '!' + srcJSFrameworksFoulder, '!' +srcJSFrameworksFoulder+'/**/*'],
    dstJS = dstIndex + '/scripts',
    //--css
    srcCsslib = ['front_end/css/*.css', 'front_end/css/*.less'],
    srcCss = ['front_end/css/custom/**/*.css', 'front_end/css/custom/**/*.less'],
    dstCss = dstIndex + '/css',
	//
    clearJsCssPath = [dstIndex + '/css/**/*', dstIndex + '/scripts/**/*',
	                             '!' + dstIndex + '/css/styles.min.css',
                                 '!' + dstIndex + '/scripts/main.min.js',
                                 '!' + dstIndex + '/scripts/angular.min.js'];

gulp.task('default', ['buildDebug'],
    function () {
        require('./server.js').server.listen(require('./server.js').port);
    });

gulp.task('clearContent', function () {
    del([dstIndex + '/**/*', '!' + dstIndex + '/images', '!' + dstIndex + '/images/**/*']);
});

gulp.task('clearJsCss', function () {
    del(clearJsCssPath);
});

gulp.task('buildRelease',
  function () {//'copyContent', 'useref', 'clearJsCss', 
	runSequence('browserSync', 'runServer');
});

gulp.task('useref', function () {
    gulp.src(dstIndex + '/index.html')
               .pipe(useref())
			   .pipe(gulp.dest(dstIndex));
			   
			   //gulp.src([dstIndex + '/scripts/**/*',
	             /*                //'!' + dstIndex + '/css/styles.min.css',
                                 '!' + dstIndex + '/scripts/main.min.js',
                                 '!' + dstIndex + '/scripts/angular.min.js'])
			   .pipe(uglify())
			   .pipe(gulp.dest(dstIndex + '/scripts/'))  
			   .pipe(gulpif('*.css', minifyCss()))*/
			   //.pipe(gulp.dest(dstIndex + '/scripts/**/*'));
				  
});

gulp.task('buildDebug',
    function () {
        runSequence('copyContent', 'watchContent', 'runServer');
    });

gulp.task('runServer',
    function () {
        require('./server.js').server.listen(require('./server.js').port);
    });
	
//load copyImages only manually
gulp.task('copyImages', function () {
    gulp.src(srcImages)
        .pipe(gulp.dest(dstImages));
});

gulp.task('copyJSlib', function () {
    gulp.src(srcJSlib)
        .pipe(gulp.dest(dstJS));
});

gulp.task('copyCsslib', function () {
    gulp.src(srcCsslib)
        .pipe(gulp.dest(dstCss));
});

gulp.task('copyIndex', function () {
    gulp.src(srcIndex)
        .pipe(gulp.dest(dstIndex));
});

gulp.task('copyIcon', function () {
    gulp.src(srcIcon)
        .pipe(gulp.dest(dstIndex));
});

gulp.task('copyViews', function () {
    gulp.src(srcViews)
        .pipe(gulp.dest(dstViews));
});

gulp.task('copyJs', function () {
    gulp.src(srcJS)
        .pipe(gulp.dest(dstJS));
});

gulp.task('copyAngularJs', function () {
    gulp.src(srcAngularJS)
        .pipe(gulp.dest(dstJS + '/angular'));
});

gulp.task('copyCss', function () {
    gulp.src(srcCss)
        .pipe(gulp.dest(dstCss));
});

gulp.task('copyContent',
    function () {
        runSequence('copyJSlib', 'copyCsslib', 'copyIndex', 'copyIcon', 'copyViews',
         'copyJs', 'copyAngularJs', 'copyCss');
    });

gulp.task('nodemon', function (cb) {
    var started = false;

    return nodemon({
        script: 'server.js'
    }).on('start', function () {
        if (!started) {
            cb();
            started = true;
        }
    });
});

gulp.task('browserSync', ['nodemon'], function () {
    browserSync.init(null, {
        proxy: 'http://localhost:' + require('./server.js').port,
        files: ['public/**/*.*', gamePath],
        port: 3232
    });
});

gulp.task('browserSyncSet', ['browserSync'], function () {
    gulp.src(srcJSlib)
        .pipe(browserSync.reload({
            stream: true
        }));

    gulp.src(srcCsslib)
        .pipe(browserSync.reload({
            stream: true
        }));

    gulp.src(srcIndex)
		.pipe(browserSync.reload({
		    stream: true
		}));

    gulp.src(srcIcon)
        .pipe(browserSync.reload({
            stream: true
        }));

    gulp.src(srcViews)
        .pipe(browserSync.reload({
            stream: true
        }));

    gulp.src(gamePath)
        .pipe(browserSync.reload({
            stream: true
        }));

    gulp.src(srcJS)
        .pipe(browserSync.reload({
            stream: true
        }));

    gulp.src(srcAngularJS)
        .pipe(browserSync.reload({
            stream: true
        }));

    gulp.src(srcCss)
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('watchContent', ['browserSyncSet'], function () {
    gulp.watch(srcJSlib, ['copyJSlib']);
    gulp.watch(srcCsslib, ['copyCsslib'])
    gulp.watch(srcIndex, ['copyIndex']);
    gulp.watch(srcIcon, ['copyIcon']);
    gulp.watch(srcViews, ['copyViews']);
    gulp.watch(gamePath);
    gulp.watch(srcJS, ['copyJs']);
    gulp.watch(srcAngularJS, ['copyAngularJs']);
    gulp.watch(srcCss, ['copyCss'])
});