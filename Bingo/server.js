﻿var http = require('http'),
    port = process.env.port || 1337,
    express = require('express'),
    io = require('socket.io');

var app = express();

require('./back_end/BLL/setConsts')(app);

var server = http.createServer(app);
io = io.listen(server);

require('./back_end/DAL/init')(app);
require('./back_end/BLL/game/gameLogic')(app, io.sockets);
require('./back_end/routers/init')(app, io.sockets);

//server.listen(port);

module.exports = { server: server, port: port, app: app };