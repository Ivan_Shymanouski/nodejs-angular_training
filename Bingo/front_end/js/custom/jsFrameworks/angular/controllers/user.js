﻿(function userCtrlInit() {
    angular.module('bingoApp').controller('userCtrl', userController);

	userController.$inject = ['$scope', '$location', '$cookieStore', 'appConsts', 'timeToDisplay', 'user', 'socketEventInit'];
	
    function userController($scope, $location, $cookieStore, consts, time, userS, sEI) {
        var self = $scope.user;
		sEI.scope = $scope;
		userS.user = self;
        self.info = {};
        self.infoList = [];
        self.time = time;        	
        self.logout = logout;        
        self.submitLoginForm = login;
        self.submitRegistrationForm = registration
        self.isLoginActive = isLoginActive;
        self.isRegistrationActive = isRegistrationActive;
        self.setActive = setActive;
        self.loginPattern = /^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$/;
        self.emailPattern = /^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/;
        self.loginOrEmailPattern = /(^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$)|(^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$)/;
        self.passwordPattern = /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;
        
        self.active = 'login';
        self.isAuthenticated = $cookieStore.get(consts.tokenName) !== undefined;
		loginByToken();
		
		function loginByToken(){
			if (self.isAuthenticated) {
				userS.asyncLogin();
				$location.path('/game').replace();
				self.active = 'game';
			} else {
				$location.path('/login').replace();
			}
		}
		
		function logout(){ userS.logout(); }
		
		function login(){ userS.login(); }
		
		function registration(){ userS.registration();   }

        function isLoginActive(){ return self.active == 'login';  }

        function isRegistrationActive() { return self.active == 'registration'; }

        function setActive(value){ self.active = value  }
    }
})();