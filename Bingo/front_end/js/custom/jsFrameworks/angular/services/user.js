(function userServiceInit(){

	angular.module('bingoApp').factory('user',userService);
	
    userService.$inject = ['$http', '$location', '$cookieStore', 'appConsts', 'showMessage', 'socket','socketEventInit', 'timeToDisplay'];
	
	function userService($http, $location, $cookieStore, consts, messageService, socket, sEI, time){
		
		var userS = {
			registration : registration,
			login : login,
			asyncLogin : asyncLogin,
			logout : logout,
			user : null
		};
		
		return userS;
		
		function registration() {
            var messageHeader = "Registration";
            
            $http({
                method: 'POST',
                url: '/registration',
                data: userS.user.info,
                headers: { 'Content-Type': 'application/json' }
            })
			.success(onRegistrationSuccess)
			.error(onRegistrationError);
			
			function onRegistrationSuccess(data) {
				if (data.isSuccess) {
					$location.path('/login').replace();
					userS.user.active = 'login';
					clearRegInfo();
					messageService.showSuccessMessage(data.Data, messageHeader);
				} else {
					messageService.showErrorMessage(data.Data, messageHeader);
				}
			}
			
			function onRegistrationError(data) {
				messageService.showErrorMessage(data, messageHeader);
			}
        }
		
		function login() {
			var messageHeader = "Login";
            
            $http({
                method: 'POST',
                url: '/login',
                data: userS.user.info,
                headers: { 'Content-Type': 'application/json' }
            })
			.success(onLoginSuccess)
			.error(onLoginError);
			
			function onLoginSuccess(data) {
				if (data.isSuccess) {
					$cookieStore.put(consts.tokenName, data.token);
					socket.connect(); sEI.initSocketEvents();
					socket.emit('loginEvent', { token: data.token });
					socket.connected = true;
					userS.user.isAuthenticated = true;
					$location.path('/game').replace();
					userS.user.active = 'game';
					messageService.showSuccessMessage(data.Data, messageHeader);
				} else {
					messageService.showErrorMessage(data.Data, messageHeader);
				}
			}
			
			function onLoginError(data) {
				messageService.showErrorMessage(data, messageHeader);
			}
		
        }
        
        function asyncLogin() {
			socket.connect(); sEI.initSocketEvents();
            socket.emit('loginEvent', { token: $cookieStore.get(consts.tokenName) });
            time.connected = true;
        }
			
		function logout() {
            var messageHeader = "Logout";
            
            $cookieStore.remove(consts.tokenName);
            userS.user.isAuthenticated = false;
			clearLogInfo()
            userS.user.active = 'login';
            messageService.showSuccessMessage("Success", messageHeader);
			socket.emit('logoutEvent', {});
			socket.sync = false;
        }
		
		function clearLogInfo(){
            userS.user.userLogin = '';
            userS.user.info.login = '';
            userS.user.info.password = '';
		}
		
		function clearRegInfo(){
			userS.user.info.repeatedPassword = '';
            userS.user.info.firstName = '';
            userS.user.info.secondName = '';
            userS.user.info.surname = '';
            userS.user.info.email = '';
		}
	}

})();