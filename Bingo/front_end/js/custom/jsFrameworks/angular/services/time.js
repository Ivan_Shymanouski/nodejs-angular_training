(function timeServiceInit(){
	angular.module('bingoApp').factory('timeToDisplay',timeService);
	
	timeService.$inject = ['socket'];
	
	function timeService(socket){
		var time = {
			minutes: 0,
            seconds: -1,
            gameId: 0,
            toString : timeToString,
			timer : timer
		};
		
		return time;
		
		function timeToString() {
            time.minutes *= 1;
            time.seconds *= 1;
            if (time.minutes < 10) time.minutes = '0' + time.minutes;
            if (time.seconds < 10 && time.seconds >= 0) time.seconds = '0' + time.seconds;
            if (socket.connected) {
                return (time.seconds == -1 && time.minutes == 0)?
					'Sync with Server':
					time.minutes + ':' + time.seconds + ' until end of game "' + time.gameId + '"';
            } else {
                return 'Server unavailable';
            }
        }
		
		function timer(scope) {
			if (socket.connected) {
				time.seconds = (time.seconds * 1) - 1;
				time.minutes = (time.minutes * 1);
				if (time.seconds < 0) {
					if (time.minutes > 0) {
						time.seconds = 59;
						time.minutes = (time.minutes * 1) - 1;
					} else {
						time.seconds = -1;
						socket.sync = false;
						return;
					}
				}
				setTimeout(function reTimer(){timer(scope)}, 1000);
			} else {
				time.seconds = 0;
				time.minutes = 0;
				socket.sync = false;
			}
			scope.$apply();
		}
	}

})();