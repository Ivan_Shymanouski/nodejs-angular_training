﻿angular.module('bingoApp', ['ngRoute', 'ngCookies']);

angular.module('bingoApp')
.constant("appConsts" ,{
	tokenName: 'BingoShimToken',
    numberCount: 97,
    playedNumberCount: 5,
	messagesCount: 20
})
.config(function ($routeProvider) {
    $routeProvider
        .when('/game', { templateUrl: 'game/game.html' })
        .when('/login', { templateUrl: 'views/authorization/login.html' })
        .when('/registration', { templateUrl: 'views/authorization/registration.html' })
        .otherwise({ redirectTo: '/' })
});