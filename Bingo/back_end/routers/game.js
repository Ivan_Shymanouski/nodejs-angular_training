﻿var express = require('express'),
    jwt = require('jsonwebtoken'),
    CheckNumbers = require('../BLL/validation/game').checkNumbers,
    GameLogic = require('../BLL/game/gameLogic'),
    GameService = require('../DAL/services/game');
var Router = express.Router();
var jwtVerifyAsync = require("bluebird").promisify(jwt.verify, jwt);

function routes(app) {
    var tokenName = app.get('tokenName'),
        secretForToken = app.get('secretForToken');    
    
    Router.post('/', authentication);

    Router.post('/', function (req, res, next) {
        jwtVerifyAsync(getToken(req) || "", secretForToken)
        .then(function (decoded) {
            var errors = [],
				numbers = req.body.numbers;

            numbers = CheckNumbers(errors, numbers);

            if (errors.length) {
                res.send({
                    isSuccess: false,
                    Data: errors
                });
                next();
				return;
			}
            var treatedGameId = GameLogic.gameId();
            GameService.getUserGame(decoded._doc._id, treatedGameId, saveUserCombinationCreate(treatedGameId, decoded, numbers, res, next));
        })
        .catch(onAuthError);
    });

    Router.get('/**', authentication);
    
    function authentication(req, res, next) {
        jwtVerifyAsync(getToken(req) || "", secretForToken)
        .then(function () { next(); })
        .catch(onAuthError);
    }
    
    function onAuthError() { res.redirect('../views/authorization/login.html'); }

    function saveUserCombinationCreate(treatedGameId, decoded, numbers, res, next) {
        return function saveUserCombination(games) {
            if (games.length === 0) {
                var game = {
                    userId: decoded._doc._id,
                    login: decoded._doc.login,
                    gameId: treatedGameId,
                    date: new Date(),
                    numbers: numbers.toString()
                };
                GameService.add(game, function afterSaveCombination(err) {
                    res.send({
                        isSuccess: !err,
                        Data: ((err) ? 'Server error. Faild to save your numbers. Try again' :
                                       'Saved to game ' + treatedGameId),
                        gameId: treatedGameId
                    });
                    next();
                });
            } else {
                numbers = numbers.toString();
                if (games[0].numbers == numbers) {
                    res.send({
                        isSuccess: false,
                        Data: 'Update was declined. You try to put same combination'
                    });
                    next();
                    return;
                }
                
                GameService.update({ userId: decoded._doc._id, gameId: treatedGameId },
							   { date: new Date(), numbers: numbers },
								function afterUpdateCombination(err) {
                    res.send({
                        isSuccess: !err,
                        Data: ((err) ? 'Server error. Faild to update your numbers. Try again' :
													'Updated to game ' + treatedGameId),
                        gameId: treatedGameId
                    });
                    next();
                });
            }
        }
    }
	
	function getToken(req) {
		var token = req.cookies[tokenName] || '""';
		try {
			token = JSON.parse(token);
		} catch (ex) {
			token = null;
		}
	
		return token;
	}
	
    return Router;
}

module.exports = routes;