﻿var express = require('express'),
    RegValidator = require('../BLL/validation/registration'),
    UserService = require('../DAL/services/user');
var router = express.Router();

function routes(app) {
    router.post('/', function registration(req, res, next) {
        var errors = [],
            email = req.body.email,
            password = req.body.password,
            repeatedPassword = req.body.repeatedPassword,
            login = req.body.login,
            firstName = req.body.firstName,
            secondName = req.body.secondName,
            surname = req.body.surname;
        
        RegValidator.validateInput(errors, email, login, password, repeatedPassword);
        
        if (errors.length) {
            res.send({
                isSuccess: false,
                Data: errors
            });
            next(); return;
        }
        
        UserService.getUserByEmailOrLogin(email ,login, function afterFindUserOnRegistration(user) {
            if (user.length) {
                res.send({
                    isSuccess: false,
                    Data: 'This user (' + login + ' or email : ' + email + ') already exists'
                });
                next();
            } else {
                var user = {
                    email: email,
                    password: password,
                    login: login,
                    firstName: firstName,
                    secondName: secondName,
                    surname: surname
                };
                
                UserService.add(user, function afterSaveNewUser(err) {
                    res.send({
                        isSuccess: !err,
                        Data: ((err) ? 'Server error. Faild to access DB (when save user). Try again' :
                                           'Success registration, ' + email)
                    });
                    next();
                });
            }
        });
    });

    return router;
}

module.exports = routes;