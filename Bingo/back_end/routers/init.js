﻿var bodyParser = require('body-parser'),
    express = require('express'),
    cookieParser = require('cookie-parser');

function init(app, sockets) {
    app.use('/', express.static('./public'));
    app.use(bodyParser.json());
    app.use(cookieParser());

    app.use('/registration', require('./registration')(app));
    app.use('/login', require('./login')(app));
    app.use('/game', require('./game')(app, sockets));
    app.use('/game', express.static('./front_end/views/game'));
}

module.exports = init;