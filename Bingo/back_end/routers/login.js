﻿var express = require('express'),
    jwt = require('jsonwebtoken'),
    validator = require('validator'),
    LoginValidator = require('../BLL/validation/login'),
    UserService = require('../DAL/services/user');
var router = express.Router();

function routes(app) {
    var timeToCoockieLife = app.get('timeToCoockieLife');

    router.post('/', function (req, res, next) {//login
        var errors = [],
            login = req.body.login,
            password = req.body.password;

        LoginValidator.validateInput(errors, login, password);

        if (errors.length) {
            res.send({
                isSuccess: false,
                Data: errors
            });
            next(); return;
        }
        
        if (validator.isEmail(login)) {
            UserService.getUserByEmailWithPassword(login, password, userFindOnSuccess);
        } else {
            UserService.getUserByLoginWithPassword(login, password, userFindOnSuccess);
        }

        function userFindOnSuccess(user) {
            if (user.length) {
                var token = jwt.sign(user[0], app.get('secretForToken'), { expiresIn: timeToCoockieLife });
                res.send({
                    isSuccess: true,
                    token: token,
                    Data: 'Success login, ' + user[0]._doc.login
                });
                next();
            } else {
                UserService.getUserByEmailOrLogin(login, login, function afterFindUserOnLogin(user) {
                    if (user.length) {
                        res.send({
                            isSuccess: false,
                            Data: 'User ( ' +
                                  ((validator.isEmail(login)) ? 'email:' : 'login:') +
                                  login + ' ) exist. Incorrect password.'
                        });
                    } else {
                        res.send({
                            isSuccess: false,
                            Data: 'There are not this (' + login + ') user in database'
                        });
                    }
                    next();
                });
            }
        }
    });

    return router;
}

module.exports = routes;