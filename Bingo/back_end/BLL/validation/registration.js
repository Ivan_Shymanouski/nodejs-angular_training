﻿var  validator = require('validator');

function validateRegistrationData(errors, email, login, password, repeatedPassword) {
    if (!validator.isEmail(email)) {
        errors.push('Incorrect email value');
    }
    if (!validator.isLogin(login)) {
        errors.push('Incorrect login value. Length of login must be greater than 1. login can be letters or numbers, the first one must be a letter');
    }
    if (!validator.isPassword(password)) {
        errors.push('Incorrect password value. Length of password must be greater than 8 and it must contain only latin and special characters and at least one of each');
    }
    else if (!(typeof repeatedPassword === "string" && repeatedPassword == password)) {
        errors.push('Incorrect repeated password value');
    }
}

module.exports = {
	validateInput : validateRegistrationData
};