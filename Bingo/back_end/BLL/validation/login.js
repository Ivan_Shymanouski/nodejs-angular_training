﻿var  validator = require('validator');

function validateLoginData(errors, login, password) {
    if (!(validator.isEmail(login) || validator.isLogin(login))) {
        errors.push('Incorrect login value. Length of login must be greater than 1. login can be letters or numbers, the first one must be a letter or it may be e-mail');
    }
    if (!validator.isPassword(password)) {
        errors.push('Incorrect password value. Length of password must be greater than 8');
    }
}

module.exports = {
	validateInput : validateLoginData
};