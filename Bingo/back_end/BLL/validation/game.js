﻿function valid_game(app, isGame) {
	var playedNumberCount = app.get('playedNumberCount');
	
	function checkNumbers(errors, numbers) {
		if (!isGame()) {
			errors.push('Game is not started. Please, try again');
		} else {
			if (!(typeof numbers === "object" && numbers instanceof Array)) {
				errors.push('Incorrect values');
			} else if (numbers.length != playedNumberCount) {
				errors.push('Incorrect size of collection. You should enter ' + playedNumberCount + ' numbers');
			} else {
				numbers.sort(function (a, b) { return a - b });
				var chekUniqArr = numbers.filter(function (e, i, a) { return i === 0 || a[i - 1] != a[i]; });
				if (chekUniqArr.length != playedNumberCount) {
					errors.push('Incorrect values. All elements must be unique if you wish win');
				}
			}
		}
		return numbers;
	}
    
    module.exports.checkNumbers = checkNumbers;

	return {
		checkNumbers : checkNumbers
	};
}

module.exports = valid_game;
