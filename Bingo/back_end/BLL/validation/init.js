﻿var validator = require('validator');

function validInit(app, isGame) {
	//vaildator extansion
    function checkValue(regExpr, value) {
        var result = false;
        if (typeof value === "string") {
            if (regExpr.test(value)) {
                result = true;
            }
        }
        return result;
    }
    
    validator.isLogin = function (value) {
        return checkValue(/^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$/, value);
    };
    validator.isPassword = function (value) {
        return checkValue(/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, value);
    };
    validator.isEmail = function (value) {
        return checkValue(/^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/, value);
    };
	
	//init
	require("./game")(app, isGame);
}

module.exports = validInit;
