﻿module.exports = function setConsts(app) {
    //app settings
    app.set('secretForToken', 'BingoShmingoPhrase');
    app.set('tokenName', 'BingoShimToken');
	app.set('countOfRecordsToView', 20);
    app.set('playedNumberCount', 5);
    app.set('numberCount', 97);
    app.set('gameDuration', 1000 * 60 * 3); //3 minutes
    app.set('syncTime', 1000 * 10); //10 seconds
    app.set('attemptsCount', 10);
    app.set('timeToCoockieLife', "7h"); //expiresIn: expressed in seconds or a string describing a time span rauchg/ms. Eg: 60, "2 days", "10h", "7d"
};