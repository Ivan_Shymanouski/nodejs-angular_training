﻿function socketRegistration(app, sockets, events){
	sockets.on('connection', function onConnectionEvent (socket) {
		for(var i=0; i<events.length; i++){
			socket.on(events[i].name, events[i].onEvent(socket));
		}
		socket.emit('loginEventRequest', {});
	});
}
	
module.exports = socketRegistration;