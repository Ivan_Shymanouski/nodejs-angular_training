﻿var BuildGameMessage = require("../common").buildGameMessage;

function sendUserCombinationCreate(socket) {
    return function sendUserCombination(items) {
        var lastCombination = [], lastGameId = '';
        if (items.length > 0) {
            try {
                lastCombination = JSON.parse('[' + items[0]._doc.numbers + ']');
            }
			catch (ex) {
                lastCombination = [];
            }
            lastGameId = items[0]._doc.gameId;
        }
        socket.emit('getLastPlayed', { numbers: lastCombination, gameId: lastGameId });
    }
}

function sendRecordsCreate(socket) {
    return function sendRecords(items) {
        var messages = items.map(function (item) {
            return BuildGameMessage(item._doc.numbers, item._doc.gameId, item._doc.message)
        });
        socket.emit('getGameHistory', messages);
    }
}

function infoUser(loginUser, name, data) {
    loginUser.socket.emit(name, data);
}

function infoAllUsers(loginUsers, name, data) {
    for (var i = 0; i < loginUsers.length; i++) {
        infoUser(loginUsers[i], name, data);
    }
}

module.exports.infoUser = infoUser;
module.exports.infoAllUsers = infoAllUsers;
module.exports.sendUserCombinationCreate = sendUserCombinationCreate;
module.exports.sendRecordsCreate = sendRecordsCreate;

module.exports = {
    infoUser : infoUser,
    infoAllUsers : infoAllUsers,
    sendUserCombinationCreate : sendUserCombinationCreate,
    sendRecordsCreate : sendRecordsCreate
}