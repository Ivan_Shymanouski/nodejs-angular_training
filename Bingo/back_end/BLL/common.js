﻿function makeProperty(initValue, _privateSet){
	var _value;
	if (typeof initValue != 'undefined'){
		_value = initValue;
	}
	
	return (typeof _privateSet != 'undefined') ? function(value, privateSet){
		if (typeof value != 'undefined'){
			if (_privateSet === privateSet){
				_value = value;
			}
		}
		else{
			return _value;
		}
	} : function(value){
		if (typeof value != 'undefined'){
			_value = value;
		}
		else{
			return _value;
		}
	}
}

function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

function buildGameMessage(numbers, gameId, message) {
	return { message: numbers + ' - win combination of game "' + gameId + '". ' + message };
}

module.exports = {
	makeProperty : makeProperty,
	getRandomInt : getRandomInt,
	buildGameMessage : buildGameMessage
};