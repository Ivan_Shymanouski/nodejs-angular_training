﻿var MakeProperty = require('../common').makeProperty,
	GameService = require('../../DAL/services/game'),
	ResultService = require('../../DAL/services/result'),
	InfoAllUsers = require('../sockets/functions').infoAllUsers;

function gameLogic(app, sockets){
	var gameDuration = app.get('gameDuration'),
        syncTime = app.get('syncTime');

	var loginUsers = MakeProperty([]), //array of {socket, userId}
		isGame = MakeProperty(false),
		startGameTime,
		gameId = MakeProperty(-1),
		timeSync = (function () {
			var syncOn = false; //to avoid memory leak
			return function (s) {
				if (!syncOn || s === syncOn) {
					syncOn = ['sync'];
                var diff = gameDuration - (new Date() - startGameTime),
						seconds = Math.floor(diff / 1000),
						minutes = Math.floor(seconds / 60);
					seconds -= (minutes * 60);
					InfoAllUsers(loginUsers(), 'sync', { minutes: minutes, seconds: seconds, gameId: ((isGame()) ? gameId() : 'sync') });
					if (loginUsers().length > 0 && isGame()) {
						setTimeout(function () { timeSync(syncOn); }, syncTime);
					} else {
						syncOn = false;
					}
				}
			}
		})();
	var gameUserId = '0';	
	
	//init system
	var events = require("./socketEvents")(app, loginUsers, isGame, gameCicle, timeSync);
	require('../sockets/registration')(app, sockets, events);
	var Calculations = require("./calculations")(app, loginUsers, gameUserId);
	require("../validation/init")(app, isGame);
    
    //getLastGame from all games
    GameService.getLastUserGame(gameUserId, getGameID);

	//using functions
	function gameCicle() {
		if (isGame()) {
			Calculations.calcGameResult(gameId());
		}

		if (loginUsers().length > 0 && gameId() > -1) {//start new game
            gameId(gameId()+1);
			setTimeout(function () { Calculations.generateNumbers(gameId()); }, 1);
			setTimeout(gameCicle, gameDuration);
			startGameTime = new Date();
			isGame(true);
			timeSync();
		} else {
			isGame(false);
		}
	}
	
	function getGameID (games) {
		var firstGameId = 0, _gameId;
		if (games.length > 0) {
            gameId(games[0]._doc.gameId);
            _gameId = gameId();
			ResultService.getRecordOfGame(_gameId, ProcessLastInteraptedGame);
			gameCicle();
		} else {
            gameId(firstGameId);
		}		
		
		function ProcessLastInteraptedGame(items) {
			if (items.length === 0) {
				Calculations.calcGameResult(_gameId);
			}
		}
    }

    module.exports.gameId = gameId;
}
module.exports = gameLogic;