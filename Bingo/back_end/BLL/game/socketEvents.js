﻿var Jwt = require('jsonwebtoken'),
	JwtVerifyAsync = require("bluebird").promisify(Jwt.verify, Jwt),
	Sockets = require('../sockets/functions'),
    GameService = require('../../DAL/services/game'),
    ResultService = require('../../DAL/services/result');

function socketEvents(app, loginUsers, isGame, gameCicle, timeSync){
	var countOfRecordsToView = app.get('countOfRecordsToView'),
        secretForToken = app.get('secretForToken');
	
	var events = [
		{ name: 'loginEvent', 
		  onEvent : function createEvent(socket){
			return function onLoginEvent (data) {
				JwtVerifyAsync(data.token || "", secretForToken)
				.then(function initUser(decoded) {
					if (!loginUsers().some(function (item) { return item.socket === socket })) {
						loginUsers().push({socket: socket, userId : decoded._doc._id});
					}
					if (!isGame()) {
						gameCicle();
					}
					timeSync();
					Sockets.infoUser(loginUsers()[loginUsers().length - 1], 'getLogin', decoded._doc.login);				
					GameService.getLastUserGame(decoded._doc._id, Sockets.sendUserCombinationCreate(socket));
                    ResultService.getLastRecords(countOfRecordsToView, Sockets.sendRecordsCreate(socket));
                    })
               .catch(function onTokenError() { });
			}
		  }
		},
		{ name: 'disconnect', 
		  onEvent : function createEvent(socket){
			return function onDisconnectEvent () {
				onDisconnect(socket);
			}
		  }
		},
		{ name: 'logoutEvent', 
		  onEvent : function createEvent(socket){
			return function onLogoutEvent () {
				onDisconnect(socket);
			}
		  }
		}
	];
	
    module.exports.events = events;
    return events;
	
	
	function onDisconnect(socket){		
		loginUsers(loginUsers().filter(function (item) { 
					if (item.socket === socket ){
						item.socket.disconnect();
					}
					return item.socket !== socket 
				}));
	}
}
module.exports = socketEvents;
