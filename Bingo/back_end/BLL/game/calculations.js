﻿var GameService = require('../../DAL/services/game'),
	ResultService = require('../../DAL/services/result'),
	Common = require('../common'),
	SocketFunc = require('../sockets/functions');
	
function calculations(app, loginUsers, gameUserId){
    var numberCount = app.get('numberCount'),
		playedNumberCount = app.get('playedNumberCount');  

	function generateNumbers(gameId) {
		var numbers = [];
		while (numbers.length < playedNumberCount) {
			numbers.push(Common.getRandomInt(1, numberCount));
			numbers.sort(function (a, b) { return a - b });
			numbers = numbers.filter(function (e, i, a) { return i === 0 || a[i - 1] != a[i]; });
		}

		var game = {
			userId: 0,
			gameId: gameId,
			numbers: numbers.toString()
		};
	
		GameService.add(game);
	}
	
	function calcGameResult(gameId) {
		GameService.getUserGame(gameUserId, gameId, _calcGameResults);
		
		function _calcGameResults(games) {
			if (games.length === 0) {
				appendGameResults('Not found', gameId, 'Game has no found');
				return;
			}
			var game = games[0];
			GameService.getUsersOfGameWithCombination(gameId, game.numbers, formGameResults);
			
			function formGameResults(users) {
				if (users.length > 1) {	
					var winners = [];
					for (var i = 0; i < users.length; i++) {
						if (users[i]._doc.userId !== gameUserId) {
							winners.push(users[i]._doc.login);
							for (var j = 0; j < loginUsers().length; j++) {
								if (loginUsers()[j].userId == users[i]._doc.userId) {
									SocketFunc.infoUser(loginUsers()[j], 'gameResult', { message: 'Congratulations. You won the game ' + gameId })
								}
							}
						}
					}
					appendGameResults(game.numbers, gameId, 'Winners: ' + winners.join(', '));
				} else {
					appendGameResults(game.numbers, gameId, 'Game has no winners');
				}
			}
		}
	}
	
	function appendGameResults(numbers, gameId, message) {
		SocketFunc.infoAllUsers(loginUsers(), 'gameResult', Common.buildGameMessage(numbers, gameId, message));
	
		var result = {
			numbers: numbers,
			gameId: gameId,
			message: message
		};
		
		ResultService.add(result);
	}
	
    module.exports.generateNumbers = generateNumbers;
    module.exports.calcGameResult = calcGameResult;
    return {
        generateNumbers : generateNumbers,
        calcGameResult : calcGameResult
    }
}

module.exports = calculations;
