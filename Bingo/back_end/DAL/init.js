﻿var mongoose = require('mongoose');

function init(app) {  
    require('./services/general')(app);
    
    //bd connections
    mongoose.connect('mongodb://localhost:27017/Bingo');
}

module.exports = init;