﻿function general(app){	
	var syncTime = app.get('syncTime'),
		attemptsCount = app.get('attemptsCount');

	function dbRequsetWithRecall(promiseF, thenF) { //recall request if error occurred, attempCount of times
		var attempt = 0;
		(function tryRequestDb() {
			attempt++;
			var promise = promiseF();
			if (typeof thenF === 'function') {
				promise.then(thenF)
			}
			promise.catch(function () {
				if (attempt <= attemptsCount) {
					setTimeout(function () { tryRequestDb(attempt); }, syncTime);
				} else {
					console.log(promiseF.name + ' error: exceeded the number of access attempts');
				}
			});
		})();
	}
	
    module.exports.dbRequsetWithRecall = dbRequsetWithRecall;
    return {
        dbRequsetWithRecall : dbRequsetWithRecall
    };
}

module.exports = general;