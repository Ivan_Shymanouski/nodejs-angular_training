﻿var User = require('../dbmodels/user'),
    General = require('./general');

function add(user, onError) {
    var _user = new User(user);
    
    General.dbRequsetWithRecall(function addUser() {
        _user.date = new Date();
        return _user.save(onError);
    });
}

function getUserByLoginWithPassword(login, password, thenF) {
    General.dbRequsetWithRecall(function _getUserByLoginWithPassword() {
        return User.find({ login: login, password: password }).exec();
    }, thenF);
}

function getUserByEmailWithPassword(email, password, thenF) {
    General.dbRequsetWithRecall(function _getUserByEmailWithPassword() {
        return User.find({ email: email, password: password }).exec();
    }, thenF);
}

function getUserByEmailOrLogin(email, login, thenF) {
    General.dbRequsetWithRecall(function _getUserByEmailOrLogin() {
        return User.find({ $or: [{ email: email }, { login: login }] }).exec();
    }, thenF);
}

module.exports = {
    add : add,
    getUserByLoginWithPassword : getUserByLoginWithPassword,
    getUserByEmailWithPassword : getUserByEmailWithPassword,
    getUserByEmailOrLogin : getUserByEmailOrLogin
};