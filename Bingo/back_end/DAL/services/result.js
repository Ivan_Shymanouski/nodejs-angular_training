﻿var Result = require('../dbmodels/result'),
    General = require('./general');

function add(result) {
    var _result = new Result(result);

    General.dbRequsetWithRecall(function addResult() {
        _result.date = new Date();
        return _result.save();
    });
}

function getLastRecords(count, thenF){
    General.dbRequsetWithRecall(function _getLastRecords() {
		return Result.find({}).sort({ 'gameId': -1 }).limit(count).exec();
	}, thenF);
}

function getRecordOfGame(gameId, thenF){
    General.dbRequsetWithRecall(function _getRecordOfGame() { 
		return Result.find({ gameId: gameId }).exec();
	}, thenF);
}

module.exports = {
	add : add,
	getLastRecords : getLastRecords,
	getRecordOfGame : getRecordOfGame
};
