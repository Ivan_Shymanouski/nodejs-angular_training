﻿var Game = require('../dbmodels/game'),
    General = require('./general');

function add(game, onError){
	var _game = new Game(game);
	
    General.dbRequsetWithRecall(function addGame() {
        _game.date = new Date();
        return _game.save(onError);
    });
}

function update(condition, newItem, thenF) {
    Game.update(condition, newItem, thenF);
}

function getLastUserGame(userId, thenF){
    General.dbRequsetWithRecall(function _getLastUserGame() {
		return Game.find({ userId:  userId}).sort({ 'gameId': -1 }).limit(1).exec();
	}, thenF);
}

function getUserGame(userId, gameId, thenF){
    General.dbRequsetWithRecall(function _getUserGame() {
		return Game.find({ userId: userId, gameId: gameId }).exec();
	},thenF);
}

function getUsersOfGameWithCombination(gameId, numbers, thenF){
    General.dbRequsetWithRecall(function _getUsersOfGameWithCombination() {
		return Game.find({ gameId: gameId, numbers: numbers }).exec();
	},thenF);
}

module.exports = {
    add : add,
    update : update,
	getLastUserGame : getLastUserGame,
	getUserGame : getUserGame,
	getUsersOfGameWithCombination : getUsersOfGameWithCombination
};