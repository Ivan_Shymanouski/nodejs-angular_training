﻿var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
    email: String,
    password: String,
    login: String,
    firstName: String,
    secondName: String,
    surname: String
});

var User = mongoose.model('User', userSchema);

module.exports = User;