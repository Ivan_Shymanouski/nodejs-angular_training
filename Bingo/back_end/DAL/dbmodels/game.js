﻿var mongoose = require('mongoose');

var gameSchema = new mongoose.Schema({
    userId: String,
    gameId: Number,
    login: String,
    date: Date,
    numbers: String
});

var Game = mongoose.model('Game', gameSchema);

module.exports = Game;
