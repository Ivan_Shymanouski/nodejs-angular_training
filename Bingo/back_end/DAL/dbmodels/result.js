﻿var mongoose = require('mongoose');

var resultSchema = new mongoose.Schema({
    numbers: String,
    gameId: Number,
    message: String,
    date: Date
});

var Result = mongoose.model('Result', resultSchema);

module.exports = Result;
