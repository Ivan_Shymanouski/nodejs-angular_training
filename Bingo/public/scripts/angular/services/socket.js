(function socketServiceInit(){

	angular.module('bingoApp').factory('socket',socketService);
	
	function socketService(){
		
		var socket;
		
		var socketObj = {
			sync : false,
			connected : false,
			on: on,
			emit : emit,
			connect : connect
		};
		
		return socketObj;
		
		function connect(){
			socket = io.connect('/');
		}
		
		function emit(name, data){
			socket.emit(name, data);
		}
		
		function on(name, data){
			socket.on(name, data);
		}
	}

})();