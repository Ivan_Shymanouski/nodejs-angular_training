(function showMessageServiceInit(){

	angular.module('bingoApp').factory('showMessage', showMessageService);
	
	function showMessageService(){
		
		var showMessageS = {
			showSuccessMessage: showSuccessMessage,
			showInfoMessage: showInfoMessage,
			showErrorMessage: showErrorMessage
		};
		
		return showMessageS;
		
		function showSuccessMessage(data, header) {
            showMessage(data, header || "", 'success');
        }
		
		function showInfoMessage(data, header) {
            showMessage(data, header || "", 'info');
        }
		
		function showErrorMessage(data, header) {
            showMessage(data, header || "", 'error');
        }
		
		function showMessage(data, header, theme) {
			if (typeof data === 'string') {
				data = [data];
			}
			if (typeof data === 'object' && data instanceof Array) {
				for (var i = 0; i < data.length; i++) {
					$.jGrowl(data[i], {
						theme: theme,
						header: header,
						position: 'bottom-right',
						life: 10000
					});
				}
			} else {
				if (theme === 'info') {
					$.jGrowl("Some UI error occure", {
						theme: 'error',
						header: header,
						position: 'bottom-right',
						life: 10000
					});
				} else {
					$.jGrowl("Some server error occure", {
						theme: 'error',
						header: header,
						position: 'bottom-right',
						life: 10000
					});
				}
			}
		}
	}

})();