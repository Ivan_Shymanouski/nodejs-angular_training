(function numbersProcessServiceInit(){

	angular.module('bingoApp').factory('numbersProcess',numbersProcessService);
	
	numbersProcessService.$inject = ['$http', 'showMessage'];
	
	function numbersProcessService($http, messageService){
		
		var NP = {
			lastSendedNumbers : [],
			sendNumbers : sendNumbers
		};
		
		return NP;
		
		function sendNumbers(game) {
            var tempMas = game.selectedBarrels.map(function (item) { return item.number; });
            $http({
                method: 'POST',
                url: '/game',
                data: { numbers: tempMas },
                headers: { 'Content-Type': 'application/json' }
            })
            .success(onSendNumbersSuccess)
            .error(onSendNumbersError);
			
			function onSendNumbersSuccess(data) {
                if (data.isSuccess) {
                    messageService.showSuccessMessage(data.Data, 'Game');
                    NP.lastSendedNumbers = game.lastSendedNumbers = tempMas;
                    game.lastSendedNumbers.gameId = data.gameId;
                } else {
                    messageService.showErrorMessage(data.Data, 'Game');
                }
            }
			
			function onSendNumbersError(data) {
                messageService.showErrorMessage(data);
			}
		}
         
	}

})();