(function socketEventInitServiceInit(){

	angular.module('bingoApp').factory('socketEventInit', socketEventInitService);
	
	socketEventInitService.$inject = ['$cookieStore', 'appConsts', 'timeToDisplay', 'socket', 'numbersProcess', 'showMessage'];
	
	function socketEventInitService($cookieStore, consts, time, socket, NP, messageService){
		
		var SEIS = {
			initSocketEvents : initSocketEvents,
			scope : null
		};
		
		return SEIS;
		
		function initSocketEvents(){
			var events = [
				{ name: 'sync', 
					onEvent : function onSyncEvent (data) {
						time.minutes = data.minutes;
						time.seconds = data.seconds;
						time.gameId = data.gameId;
						if (!socket.sync) {
							socket.sync = true;
							setTimeout(function runTimer() {time.timer(SEIS.scope) }, 1000);
						}
						SEIS.scope.$apply();
					}
				},
				{ name: 'getLogin', 
					onEvent : function onGetLoginEvent (login) {
						SEIS.scope.user.userLogin = login;
						SEIS.scope.$apply();
					}
				},
				{ name: 'loginEventRequest', 
					onEvent : function onLoginEventRequestEvent (data) {
						socket.emit('loginEvent', { token: $cookieStore.get(consts.tokenName) });
						socket.connected = true;
					}
				},
				{ name: 'getLastPlayed', 
					onEvent : function onGetLastPlayedEvent (data) {
						NP.lastSendedNumbers.length = 0;
						for (var i = 0; i < data.numbers.length; i++) {
							NP.lastSendedNumbers.push(data.numbers[i]);
						}
						NP.lastSendedNumbers.gameId = data.gameId;
						SEIS.scope.$apply();
					}
				},
				{ name: 'gameResult', 
					onEvent : function onGameResultEvent (message) {
						messageService.showInfoMessage(message.message);
						SEIS.scope.user.infoList.unshift(message);
						if (SEIS.scope.user.infoList.length > consts.messagesCount) {
							SEIS.scope.user.infoList.length = consts.messagesCount;
						}
						SEIS.scope.$apply();
					}
				},
				{ name: 'disconnect', 
					onEvent : function onDisconnectEvent () {
						socket.connected = false;
					}
				},
				{ name: 'getGameHistory', 
					onEvent : function onGetGameHistoryEvent (messages) {
						SEIS.scope.user.infoList = messages;
						SEIS.scope.$apply();
					}
				}
			];
			
			for(var i=0; i<events.length; i++){
				socket.on(events[i].name, events[i].onEvent);
			}
		}
	}

})();