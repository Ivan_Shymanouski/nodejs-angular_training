(function unselectedFilterInit() {
    angular.module('bingoApp').filter('unselected', unselectedFilter);
    
    function unselectedFilter() {
        return function (items, selected) {
            if (!(items instanceof Array)) items = [];
            if (!(selected instanceof Array)) selected = [];
            return items.filter(function (item) {
                return !selected.some(function (e) {
                    return e === item;
                });
            });
        }
    }
})();