(function invertFilterInit() {
    angular.module('bingoApp').filter('invert', invertFilter);
    
    function invertFilter() {
        return function (items) {
            var result = [];
            if (!(items instanceof Array)) items = [];
            for (var i = items.length - 1; i >= 0; i--) {
                result.push(items[i]);
            }
            return result;
        }
    }
})();