(function gameCtrlInit() {
    angular.module('bingoApp').controller('gameCtrl', gameController);

	gameController.$inject = ['$scope', 'appConsts', 'numbersProcess'];
	
    function gameController($scope, consts, NP) {
        var self = $scope.game
        self.lastSendedNumbers = NP.lastSendedNumbers;
        self.selectedBarrels = [];
        self.numbers = initNumbers();
		
        self.toogle = toogleBarrels;        
        self.submitForm = submitForm;         
        self.validLen = validLen;
		
		function initNumbers(){
			var numbers = [];
			
			for (var i = 1; i <= consts.numberCount; i++) {
				numbers.push({ number: i, selected: false });
			}
			
			return numbers;
		}
		
		function toogleBarrels(num) {
            if (self.selectedBarrels.length < consts.playedNumberCount || num.selected) {
                num.selected = !num.selected;
                if (num.selected) {
                    self.selectedBarrels.push(num);
                } else {
                    self.selectedBarrels = self.selectedBarrels.filter(function (item) { return item.number !== num.number });
                }
            }
        }
		
		function submitForm() {
           NP.sendNumbers(self);
        }
		
		function validLen(mas) {
            var result = true;
            if (mas instanceof Array) {
                result = mas.length < consts.playedNumberCount;
            }
            return result;
        }
    }
})();